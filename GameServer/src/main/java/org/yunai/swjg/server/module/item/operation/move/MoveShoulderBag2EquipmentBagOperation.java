package org.yunai.swjg.server.module.item.operation.move;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.container.EquipmentBag;
import org.yunai.swjg.server.module.item.container.ShoulderBag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

/**
 * 主背包 -> 装备背包
 * 仓库背包 -> 装备背包
 * User: yunai
 * Date: 13-6-6
 * Time: 下午7:55
 */
@Component
public class MoveShoulderBag2EquipmentBagOperation extends AbstractItemMoveOperation {

    @Override
    public boolean isSuitable(Player player, Item item, CommonBag toBag, int toIndex) {
        return (item.getBagType() == Bag.BagType.PRIM || item.getBagType() == Bag.BagType.DEPOT)
                && toBag.getBagType() == Bag.BagType.EQUIPMENT;
    }

    /**
     * 验证装备位置是否正确。若不正确，发送错误消息
     *
     * @param player  玩家信息
     * @param item    道具
     * @param toBag   目标背包
     * @param toIndex 目标背包位置
     * @return 是否能够移动
     */
    @Override
    protected boolean canMoveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        int position = EquipmentBag.getPosition(item.getType());
        if (position != toIndex) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.ITEM_EQUIP_POSITION_ERROR));
            return false;
        }
        return true;
    }

    @Override
    public boolean moveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        EquipmentBag equipmentBag = (EquipmentBag) toBag;
        return equipmentBag.equip((ShoulderBag) item.getBag(), item);
    }
}