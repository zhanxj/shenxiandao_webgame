package org.yunai.swjg.server.module.item.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.item.ItemService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_AddItemToPrimBagReq;

import javax.annotation.Resource;

/**
 * 添加道具到主背包
 * User: yunai
 * Date: 13-4-11
 * Time: 上午10:51
 */
@Controller
public class AddItemToPrimBagCommand extends GameMessageCommand<C_S_AddItemToPrimBagReq> {

    @Resource
    private ItemService itemService;

    @Override
    public void execute(Online online, C_S_AddItemToPrimBagReq msg) {
        itemService.addItemToPrimBag(online, msg.getTemplateId(), msg.getCount().intValue());
    }
}