package org.yunai.swjg.server.core.service;

import org.yunai.swjg.server.rpc.message.C_S.C_S_RepLeaveReq;
import org.yunai.yfserver.util.CollectionUtils;

import java.util.Set;

/**
 * {@link OnlineState#gaming}里的子状态
 * User: yunai
 * Date: 13-4-26
 * Time: 上午11:36
 */
public enum GamingState {

    /**
     * 无任何动作
     */
    NULL,
    /**
     * 切换到普通场景
     */
    SWITCH_NORMAL_SCENE,
    /**
     * 切换到副本场景
     */
    SWITCH_REP_SCENE,
    /**
     * 副本场景中
     */
    IN_REP_SCENE,
    /**
     * 战斗中
     */
    IN_BATTLE;

    private Set<Short> msgs;

    static {
        // TODO 消息与状态的绑定 坐等
        NULL.msgs = CollectionUtils.asSet();
        SWITCH_NORMAL_SCENE.msgs = CollectionUtils.asSet();
        SWITCH_REP_SCENE.msgs = CollectionUtils.asSet();
        IN_REP_SCENE.msgs = CollectionUtils.asSet(C_S_RepLeaveReq.CODE);
    }
}
