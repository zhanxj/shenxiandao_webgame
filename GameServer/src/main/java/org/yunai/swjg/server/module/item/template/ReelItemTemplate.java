package org.yunai.swjg.server.module.item.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.yfserver.util.CsvUtil;
import org.yunai.yfserver.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashMap;

/**
 * 卷轴道具模版
 * User: yunai
 * Date: 13-4-10
 * Time: 下午8:10
 */
public class ReelItemTemplate extends ItemTemplate {

    private static final String KEY_NEED_ITEM_BASE = "needItem";
    private static final int NEED_ITEM_MAX = 6;

    /**
     * 获得道具模版
     */
    private ItemTemplate gainTemplate;
    /**
     * 获得道具数量
     */
    private int gainCount;
    /**
     * 获得需要道具集合<br />
     * 当为装备模版时，第一个必须是装备！
     */
    private LinkedHashMap<Integer, Integer> needItems;

    public ItemTemplate getGainTemplate() {
        return gainTemplate;
    }

    public int getGainCount() {
        return gainCount;
    }

    public LinkedHashMap<Integer, Integer> getNeedItems() {
        return needItems;
    }

    // ==================== 非set/get方法 ====================
    @Override
    protected void genTemplate(CsvReader reader) throws IOException {
        gainTemplate = ItemTemplate.getItemTemplate(CsvUtil.getInt(reader, "gainTemplate", 0));
        gainCount = CsvUtil.getInt(reader, "gainCount", 0);
        needItems = new LinkedHashMap<>();
        for (int i = 1; i <= NEED_ITEM_MAX; i++) {
            int[] needItemObjs = StringUtils.splitInt(CsvUtil.getString(reader, KEY_NEED_ITEM_BASE + i, ""), ";");
            if (needItemObjs.length == 0) {
                break;
            }
            needItems.put(needItemObjs[0], needItemObjs[1]);
        }
    }

    @Override
    protected void check() {
        // 如果是装备制作卷，那合成数量必须为1，并且，第一个材料必须是装备
        // 制作卷必须叠加为1
    }
}