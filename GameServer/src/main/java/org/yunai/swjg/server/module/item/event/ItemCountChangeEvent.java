package org.yunai.swjg.server.module.item.event;

import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.yfserver.event.IEvent;

/**
 * 物品数量变化事件
 * User: yunai
 * Date: 13-5-11
 * Time: 上午1:23
 */
public class ItemCountChangeEvent implements IEvent {

    /**
     * 玩家信息
     */
    private final Player player;
    /**
     * 变化前的堆叠数
     */
    private final int oldOverlap;
    /**
     * 道具信息
     */
    private final Item item;

    public ItemCountChangeEvent(Player player, Item item, int oldOverlap) {
        this.player = player;
        this.item = item;
        this.oldOverlap = oldOverlap;
    }

    public Item getItem() {
        return item;
    }

    public int getOldOverlap() {
        return oldOverlap;
    }

    public Player getPlayer() {
        return player;
    }
}