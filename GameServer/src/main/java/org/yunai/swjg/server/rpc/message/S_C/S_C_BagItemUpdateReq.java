package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StBagItemUpdate;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20802】: 背包道具更新请求
 */
public class S_C_BagItemUpdateReq extends GameMessage {
    public static final short CODE = 20802;

    /**
     * 背包道具更新结构体数组
     */
    private List<StBagItemUpdate> items;

    public S_C_BagItemUpdateReq() {
    }

    public S_C_BagItemUpdateReq(List<StBagItemUpdate> items) {
        this.items = items;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StBagItemUpdate> getItems() {
		return items;
	}

	public void setItems(List<StBagItemUpdate> items) {
		this.items = items;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_BagItemUpdateReq struct = new S_C_BagItemUpdateReq();
		struct.setItems(getMessageList(StBagItemUpdate.Decoder.getInstance(), byteArray, StBagItemUpdate.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_BagItemUpdateReq struct = (S_C_BagItemUpdateReq) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] itemsBytes = convertMessageList(byteArray, StBagItemUpdate.Encoder.getInstance(), struct.getItems());
            byteArray.create();
            putMessageList(byteArray, itemsBytes);
            return byteArray;
        }
    }
}