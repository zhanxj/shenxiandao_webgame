package org.yunai.swjg.server.module.scene.core;

/**
 * 场景回调接口
 *
 * User: yunai
 * Date: 13-4-26
 * Time: 下午2:27
 */
public interface SceneCallable {

    /**
     * 回调方法
     */
    void call();
}
