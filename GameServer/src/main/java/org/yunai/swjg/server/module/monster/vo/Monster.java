package org.yunai.swjg.server.module.monster.vo;

import org.yunai.swjg.server.core.role.RoleDef;
import org.yunai.swjg.server.module.idSequence.IdSequenceHolder;
import org.yunai.swjg.server.module.monster.template.MonsterTemplate;
import org.yunai.swjg.server.core.role.AbstractRole;

/**
 * 怪物
 * User: yunai
 * Date: 13-5-20
 * Time: 下午10:13
 */
public class Monster extends AbstractRole {

    /**
     * 编号
     */
    private final Integer id;
    /**
     * 模版
     */
    private final MonsterTemplate template;
    /**
     * 阵形位置
     */
    private final Short position;

    public Monster(MonsterTemplate template, Short position) {
        super(RoleDef.Unit.MONSTER);
        this.id = IdSequenceHolder.genMonsterId();
        this.position = position;
        this.template = template;
        // TODO 暂时这样
        super.setHpMax(template.getHp());
        super.setHpCur(super.getHpMax());
        super.setLevel((short) 10); // TODO 模版暂时没等级
    }

    @Override
    public void setId(Integer id) {
        throw new IllegalStateException("monster.id not allow set.");
    }

    @Override
    public Integer getId() {
        return id;
    }
}
