package org.yunai.swjg.server.core.role;

import org.yunai.yfserver.object.KeyValuePair;
import org.yunai.yfserver.object.ObjectPropertyArray;

/**
 * 增加了属性类型({@link #propertyType})的属性对象<br />
 * 增加了属性类型，在获取getChanged时会自动修正key
 * User: yunai
 * Date: 13-5-2
 * Time: 下午8:51
 */
public class PropertyObject extends ObjectPropertyArray {

    protected final RoleDef.PropertyType propertyType;

    public PropertyObject(int size, RoleDef.PropertyType propertyType) {
        super(size);
        this.propertyType = propertyType;
    }

    /**
     * 根据属性索引+属性类型({@link #propertyType}获得属性KEY
     *
     * @param index 索引
     * @return 属性KEY
     */
    public int getKey(int index) {
        return propertyType.getType() * 100 + index;
    }

    /**
     * 根据属性key计算出其属性索引<br />
     * 该方法跟{@link #getKey(int)}相反
     *
     * @param key 属性key
     * @return 属性索引
     */
    public int getIndex(int key) {
        return key - propertyType.getType() * 100;
    }

    /**
     * 被修改过的属性索引以及其对应的值<br />
     * 重写父方法, 与父方法不同的是，会根据{@link #propertyType}重新计算key
     *
     * @return 改变的键值对
     */
    @Override
    public KeyValuePair<Integer, Object>[] getChanged() {
        KeyValuePair<Integer, Object>[] changed = super.getChanged();
        handleKey(changed);
        return changed;
    }

    /**
     * 所有属性索引以及其对应的值<br />
     * 重写父方法, 与父方法不同的是，会根据{@link #propertyType}重新计算key
     *
     * @return 改变的键值对
     */
    @Override
    public KeyValuePair<Integer, Object>[] getIndexValuePairs() {
        KeyValuePair<Integer, Object>[] indexValuePairs = super.getIndexValuePairs();
        handleKey(indexValuePairs);
        return indexValuePairs;
    }

    /**
     * 根据{@link #propertyType}重新计算key
     *
     * @param pairs 键值对
     */
    private void handleKey(KeyValuePair<Integer, Object>[] pairs) {
        for (KeyValuePair<Integer, Object> pair : pairs) {
            pair.setKey(getKey(pair.getKey()));
        }
    }
}
