package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StBagItemUpdate;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20801】: 背包更新请求
 */
public class S_C_BagUpdateReq extends GameMessage {
    public static final short CODE = 20801;

    /**
     * 背包编号
     */
    private Byte bagId;
    /**
     * 容量
     */
    private Byte capacity;
    /**
     * 背包道具更新结构体数组
     */
    private List<StBagItemUpdate> items;
    /**
     * 背包属于谁.目前会出现属于玩家或者伙伴
     */
    private Integer wearId;

    public S_C_BagUpdateReq() {
    }

    public S_C_BagUpdateReq(Byte bagId, Byte capacity, List<StBagItemUpdate> items, Integer wearId) {
        this.bagId = bagId;
        this.capacity = capacity;
        this.items = items;
        this.wearId = wearId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getBagId() {
		return bagId;
	}

	public void setBagId(Byte bagId) {
		this.bagId = bagId;
	}
	public Byte getCapacity() {
		return capacity;
	}

	public void setCapacity(Byte capacity) {
		this.capacity = capacity;
	}
	public List<StBagItemUpdate> getItems() {
		return items;
	}

	public void setItems(List<StBagItemUpdate> items) {
		this.items = items;
	}
	public Integer getWearId() {
		return wearId;
	}

	public void setWearId(Integer wearId) {
		this.wearId = wearId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_BagUpdateReq struct = new S_C_BagUpdateReq();
            struct.setBagId(byteArray.getByte());
            struct.setCapacity(byteArray.getByte());
		struct.setItems(getMessageList(StBagItemUpdate.Decoder.getInstance(), byteArray, StBagItemUpdate.class));
            struct.setWearId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_BagUpdateReq struct = (S_C_BagUpdateReq) message;
            ByteArray byteArray = ByteArray.createNull(6);
            byte[][] itemsBytes = convertMessageList(byteArray, StBagItemUpdate.Encoder.getInstance(), struct.getItems());
            byteArray.create();
            byteArray.putByte(struct.getBagId());
            byteArray.putByte(struct.getCapacity());
            putMessageList(byteArray, itemsBytes);
            byteArray.putInt(struct.getWearId());
            return byteArray;
        }
    }
}