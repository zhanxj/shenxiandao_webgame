package org.yunai.swjg.server.module.formation;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.formation.vo.Formation;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 阵形持久化更新器
 * User: yunai
 * Date: 13-5-31
 * Time: 上午1:32
 */
@Component
public class FormationUpdater implements PersistenceObjectUpdater {

    @Resource
    private FormationMapper formationMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((Formation) obj, formationMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        throw new UnsupportedOperationException("阵形信息不支持delete.");
    }
}