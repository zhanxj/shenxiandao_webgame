package org.yunai.swjg.server.module.scene.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.scene.NormalSceneService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_SceneSwitchReq;

import javax.annotation.Resource;

/**
 * 玩家切换普通场景命令
 * User: yunai
 * Date: 13-4-26
 * Time: 下午2:54
 */
@Component
public class NormalSceneSwitchCommand extends GameMessageCommand<C_S_SceneSwitchReq> {

    @Resource
    private NormalSceneService sceneService;

    @Override
    public void execute(Online online, C_S_SceneSwitchReq msg) {
        sceneService.onPlayerSwitchNormalScene(online, msg.getSceneId());
    }
}