package org.yunai.swjg.server.module.player;

import org.apache.ibatis.annotations.Param;
import org.yunai.swjg.server.entity.PlayerEntity;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

/**
 * 玩家数据访问类
 * User: yunai
 * Date: 13-3-29
 * Time: 上午10:43
 */
public interface PlayerMapper extends Mapper, SaveMapper<PlayerEntity> {

    /**
     * 查询玩家信息
     * @param id 玩家编号
     * @return 玩家信息
     */
    PlayerEntity selectPlayer(Integer id);

    /**
     * 根据帐号编号和服务器编号查询玩家信息
     * @param uid 帐号编号
     * @param serverId 服务器编号
     * @return 玩家信息
     */
    PlayerEntity selectPlayerByUidAndServerId(@Param("uid") Integer uid,
                                              @Param("serverId") Short serverId);

    /**
     * 根据服务器编号和昵称查询玩家编号
     * @param serverId 服务器编号
     * @param nickname 昵称
     * @return 玩家编号
     */
    Integer selectPlayerIdByServerIdAndNickname(@Param("serverId") Short serverId,
                                                @Param("nickname") String nickname);

    /**
     * 新增玩家记录
     * @param playerEntity 玩家信息
     */
    void insertPlayer(PlayerEntity playerEntity);

    /**
     * 更新玩家数据
     * @param entity 玩家数据
     */
    void update(PlayerEntity entity);
}
