package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 道具获得结构体
 */
public class StItemGet implements IStruct {
    /**
     * 道具编号
     */
    private Integer templateId;
    /**
     * 道具数量
     */
    private Integer count;

    public StItemGet() {
    }

    public StItemGet(Integer templateId, Integer count) {
        this.templateId = templateId;
        this.count = count;
    }

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StItemGet struct = new StItemGet();
            struct.setTemplateId(byteArray.getInt());
            struct.setCount(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StItemGet struct = (StItemGet) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getTemplateId());
            byteArray.putInt(struct.getCount());
            return byteArray;
        }
    }
}