package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;

/**
 * 竞技场玩家每条排行版结构体
 */
public class StArenaPlayerRank implements IStruct {
    /**
     * 战力
     */
    private Integer militaryCapability;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 趋势
     */
    private Byte trend;

    public StArenaPlayerRank() {
    }

    public StArenaPlayerRank(Integer militaryCapability, String nickname, Byte trend) {
        this.militaryCapability = militaryCapability;
        this.nickname = nickname;
        this.trend = trend;
    }

	public Integer getMilitaryCapability() {
		return militaryCapability;
	}

	public void setMilitaryCapability(Integer militaryCapability) {
		this.militaryCapability = militaryCapability;
	}
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Byte getTrend() {
		return trend;
	}

	public void setTrend(Byte trend) {
		this.trend = trend;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StArenaPlayerRank struct = new StArenaPlayerRank();
            struct.setMilitaryCapability(byteArray.getInt());
            struct.setNickname(getString(byteArray));
            struct.setTrend(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StArenaPlayerRank struct = (StArenaPlayerRank) message;
            ByteArray byteArray = ByteArray.createNull(5);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            byteArray.putInt(struct.getMilitaryCapability());
            putString(byteArray, nicknameBytes);
            byteArray.putByte(struct.getTrend());
            return byteArray;
        }
    }
}