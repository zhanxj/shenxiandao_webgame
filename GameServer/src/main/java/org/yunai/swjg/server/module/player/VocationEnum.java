package org.yunai.swjg.server.module.player;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 职业枚举
 * User: yunai
 * Date: 13-5-3
 * Time: 上午12:05
 */
public enum VocationEnum implements IndexedEnum {

    /**
     * 剑灵
     */
    JIANLING((short) 1),
    /**
     * 武圣
     */
    WUSHENG((short) 2),
    /**
     * 飞羽
     */
    FEIYU((short) 3),
    /**
     * 将星
     */
    JIANGXING((short) 4),
    /**
     * 术士
     */
    SHUSHI((short) 5);

    /**
     * 职业编号
     */
    private final short code;

    public static final List<VocationEnum> VALUES = Util.toIndexes(values());

    private VocationEnum(short code) {
        this.code = code;
    }

    @Override
    public int getIndex() {
        return code;
    }

    public static VocationEnum valueOf(short index) {
        return Util.valueOf(VALUES, index);
    }

    /**
     * @return 职业编号
     */
    public short getCode() {
        return code;
    }

    /**
     * @param code 职业编号
     * @return 是否合法
     */
    public static boolean check(short code) {
        return valueOf(code) != null;
    }
}
