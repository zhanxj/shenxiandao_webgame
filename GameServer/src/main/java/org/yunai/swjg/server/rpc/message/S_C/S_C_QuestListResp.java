package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StQuestInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21002】: 任务列表响应(进行中的任务+可接任务)
 */
public class S_C_QuestListResp extends GameMessage {
    public static final short CODE = 21002;

    /**
     * 任务数组
     */
    private List<StQuestInfo> quests;

    public S_C_QuestListResp() {
    }

    public S_C_QuestListResp(List<StQuestInfo> quests) {
        this.quests = quests;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StQuestInfo> getQuests() {
		return quests;
	}

	public void setQuests(List<StQuestInfo> quests) {
		this.quests = quests;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_QuestListResp struct = new S_C_QuestListResp();
		struct.setQuests(getMessageList(StQuestInfo.Decoder.getInstance(), byteArray, StQuestInfo.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_QuestListResp struct = (S_C_QuestListResp) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] questsBytes = convertMessageList(byteArray, StQuestInfo.Encoder.getInstance(), struct.getQuests());
            byteArray.create();
            putMessageList(byteArray, questsBytes);
            return byteArray;
        }
    }
}