package org.yunai.swjg.server.module.scene.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.player.PlayerService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_SceneEnterReq;

import javax.annotation.Resource;

/**
 * 玩家进入普通场景命令
 * User: yunai
 * Date: 13-4-22
 * Time: 下午2:25
 */
@Component
public class NormalSceneEnterCommand extends GameMessageCommand<C_S_SceneEnterReq> {

    @Resource
    private PlayerService playerService;

    @Override
    public void execute(Online online, C_S_SceneEnterReq msg) {
        playerService.enterScene(online);
    }

}
