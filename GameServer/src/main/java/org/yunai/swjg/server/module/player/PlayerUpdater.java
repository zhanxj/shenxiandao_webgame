package org.yunai.swjg.server.module.player;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.yfserver.async.IIoOperationService;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.persistence.async.SavePersistenceObjectOperation;
import org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater;

import javax.annotation.Resource;

/**
 * 玩家持久化更新器
 * User: yunai
 * Date: 13-5-4
 * Time: 上午10:19
 */
@Component
public class PlayerUpdater implements PersistenceObjectUpdater {

    @Resource
    private PlayerMapper playerMapper;
    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void save(PersistenceObject<?, ?> obj) {
        ioOperationService.asyncExecute(new SavePersistenceObjectOperation<>((Player) obj, playerMapper));
    }

    @Override
    public void delete(PersistenceObject<?, ?> obj) {
        throw new UnsupportedOperationException("玩家信息不支持delete.");
    }
}