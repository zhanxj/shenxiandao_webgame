package org.yunai.swjg.server.module.scene.core.msg;

import org.yunai.swjg.server.core.annotation.MainThread;
import org.yunai.swjg.server.module.scene.core.SceneCallable;
import org.yunai.yfserver.message.sys.SysInternalMessage;
import org.yunai.yfserver.util.Assert;

/**
 * 玩家离开场景结果消息
 *
 * User: yunai
 * Date: 13-4-26
 * Time: 上午12:09
 */
public class SysPlayerLeaveSceneResult extends SysInternalMessage {

    private final SceneCallable callback;

    public SysPlayerLeaveSceneResult(Integer playerId, SceneCallable callback) {
        Assert.notNull(callback, "callback can't be null!");
        this.callback = callback;
    }

    @Override
    @MainThread
    public void execute() {
        callback.call();
    }

    @Override
    public short getCode() {
        return 0;
    }
}
