package org.yunai.swjg.server.module.rep;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 副本枚举接口
 * User: yunai
 * Date: 13-5-16
 * Time: 上午9:07
 */
public interface RepDef {

    public static enum Type implements IndexedEnum {
        ordinary(1),
        boss(2);

        private int code;

        Type(int code) {
            this.code = code;
        }

        @Override
        public int getIndex() {
            return code;
        }

        public static final List<Type> VALUES = Util.toIndexes(values());

        public static Type valueOf(short code) {
            return Util.valueOf(VALUES, (int) code);
        }
    }

}
