package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21206】: 玩家经验增加消息
 */
public class S_C_PlayerAddExpResp extends GameMessage {
    public static final short CODE = 21206;

    /**
     * 增加经验
     */
    private Integer incrExp;

    public S_C_PlayerAddExpResp() {
    }

    public S_C_PlayerAddExpResp(Integer incrExp) {
        this.incrExp = incrExp;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getIncrExp() {
		return incrExp;
	}

	public void setIncrExp(Integer incrExp) {
		this.incrExp = incrExp;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_PlayerAddExpResp struct = new S_C_PlayerAddExpResp();
            struct.setIncrExp(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_PlayerAddExpResp struct = (S_C_PlayerAddExpResp) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getIncrExp());
            return byteArray;
        }
    }
}