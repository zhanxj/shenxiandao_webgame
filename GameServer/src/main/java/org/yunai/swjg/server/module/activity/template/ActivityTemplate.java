package org.yunai.swjg.server.module.activity.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 活动模版
 * User: yunai
 * Date: 13-5-20
 * Time: 下午3:21
 */
public class ActivityTemplate {

    /**
     * 活动编号
     */
    private Integer id;
    /**
     * 活动标题
     */
    private String title;
    /**
     * 开启时间描述<br />
     * 比如：<br />
     * 1. 挑战时间 每天10:20<br />
     * 2. 开放时间 每天20:00 - 20:30<br />
     */
    private String openTimeDesc;
    /**
     * 准备阶段需要事件，单位：分
     */
    private Integer prepareStageTime;
    /**
     * 活动时长，单位：分
     */
    private Integer processStageTime;
    /**
     * 准备时间cron字符串
     */
    private String prepareCronTime;
    /**
     * 进行时间cron字符串
     */
    private String processCronTime;
    /**
     * 结束事件cron字符串
     */
    private String endCronTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOpenTimeDesc() {
        return openTimeDesc;
    }

    public void setOpenTimeDesc(String openTimeDesc) {
        this.openTimeDesc = openTimeDesc;
    }

    public Integer getPrepareStageTime() {
        return prepareStageTime;
    }

    public void setPrepareStageTime(Integer prepareStageTime) {
        this.prepareStageTime = prepareStageTime;
    }

    public Integer getProcessStageTime() {
        return processStageTime;
    }

    public void setProcessStageTime(Integer processStageTime) {
        this.processStageTime = processStageTime;
    }

    public String getPrepareCronTime() {
        return prepareCronTime;
    }

    public void setPrepareCronTime(String prepareCronTime) {
        this.prepareCronTime = prepareCronTime;
    }

    public String getProcessCronTime() {
        return processCronTime;
    }

    public void setProcessCronTime(String processCronTime) {
        this.processCronTime = processCronTime;
    }

    public String getEndCronTime() {
        return endCronTime;
    }

    public void setEndCronTime(String endCronTime) {
        this.endCronTime = endCronTime;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, ActivityTemplate> templates;

    public static ActivityTemplate get(Integer id) {
        return templates.get(id);
    }

    public static Collection<ActivityTemplate> getList() {
        return templates.values();
    }

    public static void load() {
        CsvReader reader = null;
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/activity/activity_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                ActivityTemplate template = read(reader);
                templates.put(template.getId(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static ActivityTemplate read(CsvReader reader) throws IOException {
        ActivityTemplate template = new ActivityTemplate();
        template.id = CsvUtil.getInt(reader, "id", 0);
        template.title = CsvUtil.getString(reader, "title", "");
        template.openTimeDesc = CsvUtil.getString(reader, "openTimeDesc", "");
        template.prepareStageTime = CsvUtil.getInt(reader, "prepareStageTime", 0);
        template.processStageTime = CsvUtil.getInt(reader, "processStageTime", 0);
        template.prepareCronTime = CsvUtil.getString(reader, "prepareCronTime", "");
        template.processCronTime = CsvUtil.getString(reader, "processCronTime", "");
        template.endCronTime = CsvUtil.getString(reader, "endCronTime", "");
        return template;
    }
}
