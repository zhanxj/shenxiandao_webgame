package org.yunai.swjg.server.module.quest.condition;

import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.swjg.server.rpc.struct.StQuestCondition;

/**
 * 任务条件
 * User: yunai
 * Date: 13-5-9
 * Time: 上午12:23
 */
public interface IQuestCondition {

    /**
     * @return 任务条件类型
     */
    QuestDef.Condition getCondition();

    /**
     * @return 需要数量
     */
    Integer getCount();

    /**
     * @return 所需主体
     */
    Integer getSubject();

    /**
     * 检查玩家是否满足条件
     *
     * @param player    玩家信息
     * @param questId   任务编号
     * @param showError 是否发送错误消息
     * @return 是否满足条件
     */
    boolean isMeet(Player player, Integer questId, boolean showError);

    /**
     * 作为接受任务的条件，在满足条件并接受任务后的处理
     *
     * @param player 玩家信息
     */
    // TODO 可能暂时不需要
    // 以后如果真的要加接受条件的验证，最好，onAccept，onFinish分开，感觉不通用
    // 可接受条件的验证，比如WOW那种，使用触发的任务。这种就需要验证，是否有某个道具列.
    void onAccept(Player player);

    /**
     * 作为完成任务的条件，在满足条件并完成任务后的处理
     *
     * @param player 玩家信息
     */
    void onFinish(Player player);

    /**
     * 根据任务条件初始化当前完成数量
     *
     * @param player 玩家信息
     * @return 当前完成数量
     */
    int initParam(Player player);

    /**
     * 根据玩家信息生成该条件的完成情况
     *
     * @param player  玩家信息
     * @param questId 任务编号
     * @return 条件的完成情况
     */
    StQuestCondition genFinishCondition(Player player, Integer questId);
}
