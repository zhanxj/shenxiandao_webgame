package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20804】: 道具拾取请求
 */
public class C_S_DropItemReq extends GameMessage {
    public static final short CODE = 20804;

    /**
     * 背包编号
     */
    private Byte bagId;
    /**
     * 背包位置
     */
    private Byte bagIndex;

    public C_S_DropItemReq() {
    }

    public C_S_DropItemReq(Byte bagId, Byte bagIndex) {
        this.bagId = bagId;
        this.bagIndex = bagIndex;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getBagId() {
		return bagId;
	}

	public void setBagId(Byte bagId) {
		this.bagId = bagId;
	}
	public Byte getBagIndex() {
		return bagIndex;
	}

	public void setBagIndex(Byte bagIndex) {
		this.bagIndex = bagIndex;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_DropItemReq struct = new C_S_DropItemReq();
            struct.setBagId(byteArray.getByte());
            struct.setBagIndex(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_DropItemReq struct = (C_S_DropItemReq) message;
            ByteArray byteArray = ByteArray.createNull(2);
            byteArray.create();
            byteArray.putByte(struct.getBagId());
            byteArray.putByte(struct.getBagIndex());
            return byteArray;
        }
    }
}