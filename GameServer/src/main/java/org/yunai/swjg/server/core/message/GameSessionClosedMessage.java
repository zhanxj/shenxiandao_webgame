package org.yunai.swjg.server.core.message;

import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.core.session.GameSession;
import org.yunai.swjg.server.module.player.PlayerService;
import org.yunai.yfserver.message.sys.SessionClosedMessage;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 游戏Session会话关闭消息<br />
 * 1.用于销毁Online
 * User: yunai
 * Date: 13-3-27
 * Time: 下午2:37
 */
public class GameSessionClosedMessage extends SessionClosedMessage<GameSession> {

    private static OnlineContextService onlineContextService;
    private static PlayerService playerService;

    static {
        GameSessionClosedMessage.onlineContextService = BeanManager.getBean(OnlineContextService.class);
        GameSessionClosedMessage.playerService = BeanManager.getBean(PlayerService.class);
    }

    public GameSessionClosedMessage(GameSession session) {
        this.session = session;
    }

    @Override
    public void execute() {
        /**
         * 目前session.getOnline返回空的原因有以下几种:
         * 1. GameServerRuntime.open为false时，导致新连接进来的online不存在
         * 2. 玩家在发出登录消息的时候，在主消息队列消息比较多的情况下，导致GameSessionOpenedMessage无法执行到的时候，也会出现这个情况
         *
         * ps：此处[情况2]不会发生
         */
        if (session.getOnline() != null) {
            playerService.onPlayerLogout(session.getOnline());
        }
    }
}

