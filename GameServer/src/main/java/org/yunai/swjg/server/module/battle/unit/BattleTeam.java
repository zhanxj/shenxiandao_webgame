package org.yunai.swjg.server.module.battle.unit;

import java.util.ArrayList;
import java.util.List;

/**
 * 战斗队伍
 * User: yunai
 * Date: 13-5-23
 * Time: 下午4:39
 */
public class BattleTeam {

    private List<BattleUnit> units = new ArrayList<>(5);

    public void addUnit(BattleUnit unit) {
        units.add(unit);
    }

    public List<BattleUnit> getUnits() {
        return units;
    }
}
