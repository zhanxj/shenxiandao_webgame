package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StItemGet;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20809】: 玩家道具获得消息
 */
public class S_C_ItemGetResp extends GameMessage {
    public static final short CODE = 20809;

    /**
     * 获得道具数组
     */
    private List<StItemGet> items;

    public S_C_ItemGetResp() {
    }

    public S_C_ItemGetResp(List<StItemGet> items) {
        this.items = items;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StItemGet> getItems() {
		return items;
	}

	public void setItems(List<StItemGet> items) {
		this.items = items;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ItemGetResp struct = new S_C_ItemGetResp();
		struct.setItems(getMessageList(StItemGet.Decoder.getInstance(), byteArray, StItemGet.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ItemGetResp struct = (S_C_ItemGetResp) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] itemsBytes = convertMessageList(byteArray, StItemGet.Encoder.getInstance(), struct.getItems());
            byteArray.create();
            putMessageList(byteArray, itemsBytes);
            return byteArray;
        }
    }
}