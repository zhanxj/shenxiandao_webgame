package _java.lang;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 数组测试
 * User: yunai
 * Date: 13-5-22
 * Time: 下午10:41
 */
public class ArrayListTest {

    @Test
    public void testAdd() {
        List<Integer> scores = new ArrayList<>(11);
        scores.add(0, 1);
        scores.add(0, 2);
        scores.set(10, 2);

        System.out.println(JSON.toJSONString(scores));
    }
}
