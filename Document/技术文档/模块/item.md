## 背包分类 ##
- 主背包（玩家）
- 装备背包（玩家、伙伴）
- 仓库背包（玩家）
- *命格主背包*（玩家）
- *命格临时背包*（玩家）
- *命格装备背包*（玩家、伙伴）
- *回购背包*（玩家）
- 封灵（暂时没想法）

----------

## 道具分类 ##
- 装备：
	1. *强化等级*（未完成）
	2. *封灵*（未完成） 
- 消耗品：
	1. *变身卷轴*（未完成）
	2. *加属性丹药*（未完成）
- 卷轴：
	1. 武器卷轴
	2. 丹药卷轴
- 材料
- *物品包*（未完成）

----------

## 使用道具 ##
- 装上装备
- 卸下装备
- 使用装备合成卷
- 使用丹药合成卷
- *装上命格*（未完成）
- *卸下命格*（未完成）
- *使用丹药*（未完成）
- *使用礼包*（未完成）
- *使用变身卷轴*（未完成）

## 移动道具 ##
- 装备背包<-> 主背包
- 装备背包 <-> 仓库背包
- 主背包 <-> 仓库背包

## 丢弃道具 ##
- 只能丢弃主背包、仓库背包的道具

## 整理背包 ##
- 只能整理主背包、仓库背包

## *增加背包容量* （未完成）##
- 只有主背包、仓库背包可以添加
