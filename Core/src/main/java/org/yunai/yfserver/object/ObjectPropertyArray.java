package org.yunai.yfserver.object;

/**
 * 对象属性数组
 * User: yunai
 * Date: 13-5-2
 * Time: 下午7:46
 */
public class ObjectPropertyArray extends PropertyArray<Object> {

    public ObjectPropertyArray(int size) {
        super(Object.class, size);
    }

    public void setLong(int index, Long val) {
        super.set(index, val);
    }

    public long getLong(int index, Long defaultVal) {
        Long val = (Long) super.get(index);
        return val != null ? val : defaultVal;
    }

    public void setInt(int index, Integer val) {
        super.set(index, val);
    }

    public int getInt(int index, int defaultVal) {
        Integer val = (Integer) super.get(index);
        return val != null ? val : defaultVal;
    }

    public void setShort(int index, Short val) {
        super.set(index, val);
    }

    public short getShort(int index, short defaultVal) {
        Short val = (Short) super.get(index);
        return val != null ? val : defaultVal;
    }

    public void setString(int index, String str) {
        super.set(index, str);
    }

    public String getString(int index, String defaultStr) {
        String str = (String) super.get(index);
        return str != null ? str : defaultStr;
    }

    public void setDouble(int index, Double val) {
        super.set(index, val);
    }

    public double getDouble(int index, double defaultVal) {
        Double val = (Double) super.get(index);
        return val != null ? val : defaultVal;
    }
}
