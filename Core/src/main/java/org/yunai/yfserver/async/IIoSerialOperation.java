package org.yunai.yfserver.async;

/**
 * 串行IO操作接口
 * @see IIoOperation
 * User: yunai
 * Date: 13-5-21
 * Time: 下午8:07
 */
public interface IIoSerialOperation extends IIoOperation {

    /**
     * @return 串行KEY
     */
    Integer getSerialKey();
}