package org.yunai.yfserver.async;

import org.yunai.yfserver.message.MessageType;
import org.yunai.yfserver.message.schedule.ScheduledMessage;

/**
 * 调度执行的异步消息通知<br />
 * 主要用于当异步消息处理完毕后，通知由主线程进行收尾处理
 * User: yunai
 * Date: 13-3-28
 * Time: 下午3:47
 */
public class ScheduleAsyncFinishMessage extends ScheduledMessage {

    private final AsyncIoOperation operation;

    public ScheduleAsyncFinishMessage(long createTimestamp, AsyncIoOperation operation) {
        super(createTimestamp);
        this.operation = operation;
    }

    @Override
    public short getCode() {
        return MessageType.SCHD_ASYNC_FINISH;
    }

    @Override
    public void execute() {
        operation.execute();
    }
}
