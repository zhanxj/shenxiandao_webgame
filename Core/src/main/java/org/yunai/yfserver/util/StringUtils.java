package org.yunai.yfserver.util;

import java.util.Map;

/**
 * 字符串工具类
 * User: yunai
 * Date: 13-5-9
 * Time: 上午1:47
 */
public class StringUtils {

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    /**
     * 分隔字符串，并返回分隔字符串数组<br />
     * 当str为null或者""时，则返回数组长度为0的字符串数组
     *
     * @param str   字符串
     * @param regex 分隔符
     * @return 分隔后的字符串数组
     */
    public static String[] split(String str, String regex) {
        if (isEmpty(str)) {
            return new String[0];
        }
        return str.split(regex);
    }

    /**
     * 分隔字符串，并返回分隔后的整数数组<br />
     * <b>该方法不检查是否非整数，请保证穿进来的字符串分隔后都是整数</b>
     * 当str为null或""时，则返回数组长度为0的整数数组
     *
     * @param str   字符串
     * @param regex 分隔符
     * @return 分隔的整数数组
     */
    public static int[] splitInt(String str, String regex) {
        String[] strs = split(str, regex);
        if (strs.length == 0) {
            return new int[0];
        }
        int[] arr = new int[strs.length];
        for (int i = 0, len = strs.length; i < len; i++) {
            arr[i] = Integer.valueOf(strs[i]);
        }
        return arr;
    }

    /**
     * 分隔字符串，并返回分隔后的整数数组<br />
     * <b>该方法不检查是否非整数，请保证穿进来的字符串分隔后都是整数</b>
     * 当str为null或""时，则返回数组长度为0的整数数组
     *
     * @param str   字符串
     * @param regex 分隔符
     * @return 分隔的整数数组
     */
    public static short[] splitShort(String str, String regex) {
        String[] strs = split(str, regex);
        if (strs.length == 0) {
            return new short[0];
        }
        short[] arr = new short[strs.length];
        for (int i = 0, len = strs.length; i < len; i++) {
            arr[i] = Short.valueOf(strs[i]);
        }
        return arr;
    }

    /**
     * 根据整数数组+分隔符创建字符串
     *
     * @param arr   整数数组
     * @param regex 分隔符
     * @return 字符串
     */
    public static String buildString(int[] arr, String regex) {
        if (arr.length == 0) {
            return "";
        }
        String str = String.valueOf(arr[0]);
        for (int i = 1, len = arr.length; i < len; i++) {
            str += regex + arr[i];
        }
        return str;
    }

    /**
     * 将集合转化为字符串
     *
     * @param map    集合
     * @param regex1 key/value之间的分隔符
     * @param regex2 每个entry之间的分隔符
     * @return 字符串
     */
    public static String buildString(Map<?, ?> map, String regex1, String regex2) {
        if (map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append(regex2);
            }
            sb.append(entry.getKey()).append(regex1).append(entry.getValue());
        }
        return sb.toString();
    }
}
