package org.yunai.yfserver.util;

/**
 * 内存工具类
 * User: yunai
 * Date: 13-4-27
 * Time: 上午11:22
 */
public class MemoryUtils {

    private static final int MB = 1024 * 1024;

    /**
     * @return 已使用内存
     */
    public static long usedMemoryMB() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.freeMemory() - runtime.totalMemory()) / MB;
    }

    /**
     * @return 剩余内存
     */
    public static long freeMemoryMB() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.maxMemory() -  runtime.totalMemory() + runtime.freeMemory()) / MB;
    }

    /**
     * @return 总共内存
     */
    public static long maxMemoryMB() {
        return Runtime.getRuntime().maxMemory() / MB;
    }
}
