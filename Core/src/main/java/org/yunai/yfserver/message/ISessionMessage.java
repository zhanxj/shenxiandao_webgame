package org.yunai.yfserver.message;

import org.yunai.yfserver.session.ISession;

/**
 * 基于会话的消息
 * User: yunai
 * Date: 13-3-26
 * Time: 上午12:23
 */
public interface ISessionMessage<S extends ISession> extends IMessage {

    /**
     * 获得会话
     * @return
     */
    S getSession();

    /**
     * 设置会话
     * @param session 会话
     */
    void setSession(S session);
}
