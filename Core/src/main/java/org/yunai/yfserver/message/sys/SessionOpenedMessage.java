package org.yunai.yfserver.message.sys;

import org.yunai.yfserver.message.ISessionMessage;
import org.yunai.yfserver.message.MessageType;
import org.yunai.yfserver.session.ISession;

/**
 * Session创建内部消息
 * User: yunai
 * Date: 13-3-26
 * Time: 下午11:19
 */
public class SessionOpenedMessage<S extends ISession>
        extends SysInternalMessage
        implements ISessionMessage<S> {

    protected S session;

    @Override
    public short getCode() {
        return MessageType.SYS_SESSION_OPEN;
    }

    @Override
    public void execute() {
    }

    @Override
    public S getSession() {
        return session;
    }

    @Override
    public void setSession(S session) {
        this.session = session;
    }
}
