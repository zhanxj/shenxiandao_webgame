package org.yunai.yfserver.command;

/**
 * 命令接口
 * User: yunai
 * Date: 13-3-15
 * Time: 上午9:42
 */
public interface Command {

    /**
     * 消息编号 - 读空闲
     */
    public static final short CODE_SESSION_READER_IDLE = 101;
    /**
     * 消息编号 - 写空闲
     */
    public static final short CODE_SESSION_WRITER_IDLE = 102;
    /**
     * 消息编号 - 读写空闲
     */
    public static final short CODE_SESSION_BOTH_IDLE = 103;
    /**
     * 消息编号 - 连接打开
     */
    public static final short CODE_SESSION_OPENED = 104;
    /**
     * 消息编号 - 连接关闭
     */
    public static final short CODE_SESSION_CLOSED = 105;

    /**
     * 消息编号 - 每秒定时任务
     */
    public static final short CODE_TIMER_EACH_SECOND = 200;
    /**
     * 消息编号 - 每分钟定时任务
     */
    public static final short CODE_TIMER_EACH_MINUTE = 201;
    /**
     * 消息编号 - 每小时定时任务
     */
    public static final short CODE_TIMER_EACH_HOUR = 202;
    /**
     * 消息编号 - 每天定时任务
     */
    public static final short CODE_TIMER_EACH_DAY = 203;

    /**
     * 命令对应的消息编号
     * @return 消息编号
     */
    short getCode();
}
