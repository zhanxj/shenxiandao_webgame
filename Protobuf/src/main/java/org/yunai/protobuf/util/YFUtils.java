package org.yunai.protobuf.util;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class YFUtils {

    private static Set<String> notImportSet;

    static {
        notImportSet = new HashSet<String>();
        notImportSet.add(Byte.class.getSimpleName());
        notImportSet.add(Short.class.getSimpleName());
        notImportSet.add(Integer.class.getSimpleName());
        notImportSet.add(Long.class.getSimpleName());
        notImportSet.add(Float.class.getSimpleName());
        notImportSet.add(Double.class.getSimpleName());
        notImportSet.add(String.class.getSimpleName());
    }

    public static boolean notImportSetExists(String simpleName) {
        return notImportSet.contains(simpleName);
    }

    /**
     * 参数类型 - 单个
     */
    public static final int PARAM_TYPE_ONE = 1;
    /**
     * 参数类型 - 数组
     */
    public static final int PARAM_TYPE_LIST = 2;
    /**
     * 数组参数类型的开头
     */
    public static final String PARAM_TYPE_LIST_START_STR = "Array.";

    /**
     * 基本数据类型. char/byte/short/int/long/float/double
     */
    public static final int DATA_TYPE_SIMPLE = 1;
    /**
     * 字符串数据类型. String
     */
    public static final int DATA_TYPE_STRING = 2;
    /**
     * 消息数据类型(即, 消息类型). Message接口的实现类
     */
    public static final int DATA_TYPE_STRUCT = 3;

    /**
     * 根据type转化为其在JAVA里的基本数据类型
     *
     * @param type 配置文件里定义的type
     * @return
     */
    public static String convertDataType(String type) {
        return "byte".equals(type) ? "byte" :
                "char".equals(type) ? "char" :
                        "short".equals(type) ? "short" :
                                "int".equals(type) ? "int" :
                                        "long".equals(type) ? "long" :
                                                "float".equals(type) ? "float" :
                                                        "double".equals(type) ? "double" :
                                                                "string".equals(type) ? "String" : type;
    }

    /**
     * 根据type转化为其在JAVA里的对象数据类型
     *
     * @param paramType 配置文件里的type
     * @return JAVA里的对象数据类型
     */
    public static String convertDataObject(String paramType) {
        return "Char".equals(paramType) ? "Character" :
               "Int".equals(paramType) ? "Integer" :
               paramType.startsWith(PARAM_TYPE_LIST_START_STR) ?
                       paramType.substring(PARAM_TYPE_LIST_START_STR.length()) : paramType;
    }

    /**
     * 根据type获得其字节长度
     *
     * @param paramType 参数类型
     * @return 字节长度
     */
    public static int calParamLength(String paramType) {
        return Byte.class.getSimpleName().equals(paramType) ? 1 :
               Short.class.getSimpleName().equals(paramType) ? 2 :
               "Char".equals(paramType) ? 2 :
               "Int".equals(paramType) ? 4 :
               Long.class.getSimpleName().equals(paramType) ? 8 :
               Float.class.getSimpleName().equals(paramType) ? 4 :
               Double.class.getSimpleName().equals(paramType) ? 8 : -1;
    }

    /**
     * 根据参数(Param)类型返回它的数据定义编号
     *
     * @param type 参数类型
     * @return 数据定义编号
     */
    public static int defDataType(String type) {
        return Byte.class.getSimpleName().equals(type) ? DATA_TYPE_SIMPLE :
               Short.class.getSimpleName().equals(type) ? DATA_TYPE_SIMPLE :
               "Char".equals(type) ? DATA_TYPE_SIMPLE :
               "Int".equals(type) ? DATA_TYPE_SIMPLE :
               Long.class.getSimpleName().equals(type) ? DATA_TYPE_SIMPLE :
               Float.class.getSimpleName().equals(type) ? DATA_TYPE_SIMPLE :
               Double.class.getSimpleName().equals(type) ? DATA_TYPE_SIMPLE :
               String.class.getSimpleName().equals(type) ? DATA_TYPE_STRING : DATA_TYPE_STRUCT;
    }

    /**
     * 根据参数(Param)类型返回它的类型定义编号
     *
     * @param paramType 参数类型
     * @return 类型定义编号
     */
    public static int defParamType(String paramType) {
        return paramType.startsWith(PARAM_TYPE_LIST_START_STR) ? PARAM_TYPE_LIST : PARAM_TYPE_ONE;
    }

    /**
     * 将字符串首字母变成大写<br />
     * ×× 请保证首字母在['a', 'z']内
     *
     * @param str
     * @return
     */
    public static String convertFirst2Big(String str) {
        byte[] bytes = str.getBytes();
        bytes[0] = (byte) (bytes[0] - 'a' + 'A');
        return new String(bytes);
    }

    /**
     * 将字符串首字母变成小写<br />
     * ×× 请保证首字母在['A', 'Z']内
     *
     * @param str
     * @return
     */
    public static String convertFirst2Small(String str) {
        byte[] bytes = str.getBytes();
        bytes[0] = (byte) (bytes[0] + 'a' - 'A');
        ;
        return new String(bytes);
    }

    /**
     * 关闭读写流
     *
     * @param closeable
     * @throws java.io.IOException
     */
    public static void closeIo(Closeable closeable) throws IOException {
        if (null != closeable) {
            closeable.close();
            closeable = null;
        }
    }

    /**
     * 关闭一堆读写流
     *
     * @param closeables
     * @throws java.io.IOException
     */
    public static void closeIo(Closeable... closeables) throws IOException {
        for (Closeable closeable : closeables) {
            closeIo(closeable);
        }
    }
}
