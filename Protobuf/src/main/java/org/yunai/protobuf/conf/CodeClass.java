package org.yunai.protobuf.conf;

import java.util.List;

/**
 * 生成代码的接口
 * User: yunai
 * Date: 13-3-14
 * Time: 上午9:40
 */
public interface CodeClass {

    /**
     * 获得参数列表
     * @return 参数列表
     */
    List<Param> obtainParams();

    /**
     * 获得类名
     * @return 类名
     */
    String obtainClassName();
}
